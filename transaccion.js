const formulario = document.getElementById('formulario');
const inputs=document.querySelectorAll('#formulario input');

const expresiones = {
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
	telefono: /^\d{7,11}$/ // 7 a 14 numeros.	
}

function fechas() {
    var nacimiento = new Date(document.getElementById("date").value) ;
	var today = new Date();
	var eee=nacimiento.getFullYear();
	var yyyy = today.getFullYear();
	let agnios=yyyy-eee;
	if(agnios<18){
		console.log('false');
		document.querySelector('#grupo__fecha .formulario__input-error').classList.add('formulario__input-error-activo');
		return false;
	}else{
		console.log('true');
		document.querySelector('#grupo__fecha .formulario__input-error').classList.remove('formulario__input-error-activo');
		return true;
		
	}
}
const campos = {
	fecha: false,
	nombre: false,
	url_c: false,
	email: false,
	telefono: false,
	descripcion:false
}
const ValidarFormulario =(e)=>{
 
switch(e.target.name) {
	case "nombre":
		if(expresiones.nombre.test(e.target.value)){
			document.querySelector('#grupo__nombre .formulario__input-error').classList.remove('formulario__input-error-activo');
			document.getElementById('name').classList.remove('controls-error');
			document.getElementById('name').classList.add('controls');
			campos['nombre'] = true;
			console.log(campos['nombre']);
		}else{
			document.getElementById('name').classList.remove('controls');
			document.getElementById('name').classList.add('controls-error');
			document.querySelector('#grupo__nombre .formulario__input-error').classList.add('formulario__input-error-activo');
			campos['nombre'] = false;
			console.log(campos['nombre']);
		}
		break;
	case "email":
		if(expresiones.correo.test(e.target.value)){
			document.querySelector('#grupo__correo .formulario__input-error').classList.remove('formulario__input-error-activo');
			document.getElementById('email').classList.remove('controls-error');
			document.getElementById('email').classList.add('controls');
			campos['email'] = true;
		}else{
			document.getElementById('email').classList.remove('controls');
			document.getElementById('email').classList.add('controls-error');
			document.querySelector('#grupo__correo .formulario__input-error').classList.add('formulario__input-error-activo');
			campos['email'] = false;
		}
		break;
		case "telefono":
			if(expresiones.telefono.test(e.target.value)){
				document.querySelector('#grupo__telefono .formulario__input-error').classList.remove('formulario__input-error-activo');
				document.getElementById('telefono').classList.remove('controls-error');
				document.getElementById('telefono').classList.add('controls');
				campos['telefono'] = true;
			}else{
				document.getElementById('telefono').classList.remove('controls');
				document.getElementById('telefono').classList.add('controls-error');
				document.querySelector('#grupo__telefono .formulario__input-error').classList.add('formulario__input-error-activo');
				campos['telefono'] = false;
			}			
	};
}
function url(){
var url=document.getElementById("url").value;
if(document.getElementById("url").value==0){
	return false;
}else{
	return true;
}
}

function descripcions(){
	if(document.getElementById("comentarios").value==0){
		return false;
	}else{
		return true;
	}
}
	
function enviar(){
	const terminos = document.getElementById('terminos');
	var direccion=url();
	var fecha=fechas();
	var nombre =campos['nombre'];
	var email =campos['email'];
	var telefono=campos['telefono']
	var descripcion=descripcions();
	if(direccion!=false && fecha!=false && nombre!=false&&descripcion!=false&&email!=false&&telefono!=false &&terminos.checked ){
		document.querySelector('#enviar .formulario__input-error').classList.remove('formulario__input-error-activo');
		formulario.reset();
return(alert('Gracias por rellenar el formulario!! '));
	}else{
		document.querySelector('#enviar .formulario__input-error').classList.add('formulario__input-error-activo');
	}
}

inputs.forEach((input) => {
	input.addEventListener('keyup',ValidarFormulario);
	input.addEventListener('blur', ValidarFormulario);
});

formulario.addEventListener('submit',(e) =>{
e.preventDefault();
});
